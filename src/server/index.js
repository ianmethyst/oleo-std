const osc = require('osc');
const app = require('express')();
const server = require('http').Server(app);
const io = require('socket.io')(server);

server.listen(8080);

app.get('/', (req, res) => {
  res.sendFile(`${__dirname}/index.html`);
});

io.on('connection', (socket) => {
  socket.emit('news', { hello: 'world' });
  socket.on('my other event', (data) => {
    console.log(data);
  });
});

// Create an osc.js UDP Port listening on port 57121.
const udpPort = new osc.UDPPort({
  localAddress: '0.0.0.0',
  localPort: 5555,
  metadata: true
});

// Listen for incoming OSC bundles.
udpPort.on('message', (message, timeTag) => {
  console.log(`[${timeTag}] Broadcasting message: ${message}`);
  io.emit('osc', message);
});

// Open the socket.
udpPort.open();

udpPort.on('ready', () => {
  console.log('Ready');
});
