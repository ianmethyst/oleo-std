export function getMins(hour) {
  // Take a string with the format "xx:xx". Multiply the first number by 60 and add the second one
  const splitted = hour.split(':');
  const minutes = parseInt(splitted[0] * 60, 10) + parseInt(splitted[1], 10);

  return minutes;
}

export function constrain(n, low, high) {
  // Function from p5.js to constrain a number between a range
  return Math.max(Math.min(n, high), low);
}

export function map(n, start1, stop1, start2, stop2, withinBounds) {
  // Function from p5.js to map a number from a range to a different one
  const newval = (n - start1) / (stop1 - start1) * (stop2 - start2) + start2;
  if (!withinBounds) {
    return newval;
  }
  if (start2 < stop2) {
    return constrain(newval, start2, stop2);
  }

  return constrain(newval, stop2, start2);
}
