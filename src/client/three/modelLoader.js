import * as THREE from 'three';
import GLTFLoader from 'three-gltf-loader';

export default function modelLoader(model) {
  const loader = new GLTFLoader();

  return new Promise((resolve, reject) => {
    loader.load((model), (object) => {
      resolve(object);
    }, (xhr) => {
      /* console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' ) */
    }, (error) => {
      console.log('An error happened while loading models');
      reject(error);
    });
  });
}
