import * as THREE from 'three';

export function buildCamera(dimensions) {
  const { width, height } = dimensions;
  const camera = new THREE.PerspectiveCamera(50, width / height, 0.1, 2000);
  return camera;
}

export function buildLights() {
  const lights = [];

  const pointLight = new THREE.PointLight(0xffffff, 2, 100);
  pointLight.position.set(25, 50, 25);
  lights.push(pointLight);

  const ambientLight = new THREE.AmbientLight(0x404040, 0.3);
  lights.push(ambientLight);

  return lights;
}


export function buildRenderer(dimensions) {
  const { width, height } = dimensions;
  const renderer = new THREE.WebGLRenderer({ alpha: true, antialias: true });
  renderer.setSize(width, height);
  renderer.vr.enabled = true;
  renderer.setClearColor(0x000000, 0);

  renderer.gammaOutput = true;
  renderer.gammaFactor = 2.2;

  return renderer;
}

export function buildCurtain(opacity) {
  const geometry = new THREE.PlaneBufferGeometry(10, 10);
  const material = new THREE.MeshBasicMaterial({
    color: 0x000000,
    depthTest: false,
    transparent: true,
    opacity
  });

  const mesh = new THREE.Mesh(geometry, material);
  mesh.renderOrder = 999;
  mesh.position.set(0, 0, -0.2);
  return mesh;
}

export const reticulumConfig = {
  proximity: false,
  clickevents: true,
  near: 0.1,
  far: 20,
  reticle: {
    visible: true,
    restPoint: 1,
    color: 0xffffff,
    innerRadius: 0.006,
    outerRadius: 0.012,
    hover: {
      color: 0xffffff,
      innerRadius: 0.012,
      outerRadius: 0.024,
      speed: 5,
      vibrate: 0
    }
  },
  fuse: {
    visible: false,
    duration: 2,
    color: 0xffffff,
    innerRadius: 0.045,
    outerRadius: 0.06,
    vibrate: 0,
    clickCancelFuse: true
  }
};

export function buildScene() {
  const scene = new THREE.Scene({
    background: new THREE.Color('#000000')
  });

  return scene;
}
