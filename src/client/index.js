import './.rhl';
import React from 'react';
import ReactDOM from 'react-dom';
import { injectGlobal } from 'styled-components';

import App from './components/App';

import 'normalize.css';
import 'typeface-kalam';

injectGlobal`
  * {
    box-sizing: border-box;
  }

  html, body {
    overflow: hidden;
    margin: 0;
    padding: 0;
    width: 100vw;
    height: 100vh;

    text-align: center; 

    background: black;
    color: white;
    font-family: 'Kalam', cursive;
    overflow: hidden;
  }
`;

ReactDOM.render(<App />, document.getElementById('app'));
