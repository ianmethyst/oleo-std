import React from 'react';

import { Background } from './Elements';

const errorText = `
Parece que ocurrió un error. Recargá la
página para volver a intentar. ¡Perdón
por las molestias!
`;

const Error = () => (
  <Background>
    <h2> ¡Oh no! </h2>
    <p>
      {errorText}
    </p>
  </Background>
);

export default Error;
