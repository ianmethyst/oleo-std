import React from 'react';
import PropTypes from 'prop-types';

import HelpImg from '../img/help_i.png';

import {
  Background,
  Title,
  Subtitle,
  Rule,
  Wrapper,
  Header,
  Image,
  List,
  Text,
  Button
} from './Elements';

const helpText = `
Ordená las piezas sobre la mesa. Si alguien ya
lo está haciendo, regresá a la página anterior y
apuntá a la pared donde están las esculturas con 
la cámara de tu celular.
`;

const Help = (props) => {
  const { handleButton } = props;
  return (
    <Background opacity={0.72}>
      <Wrapper>
        <Header>
          <Button type="button" onClick={handleButton}>X</Button>
          <Title>
            Óleo de las Ideas
          </Title>
        </Header>
        <Rule />
        <Subtitle>Cómo interactuar</Subtitle>
        <Image src={HelpImg} />
        <Text>
          {helpText}
        </Text>
        <Rule />
        <h2>Créditos</h2>
        <List>
          <li>Dante Szumilo</li>
          <li>Ian Lihuel Demian Mancini Perez</li>
          <li>Julieta Iglesias Santandreu</li>
          <li>Julian Viggiano</li>
          <li>Lautaro Valdez</li>
          <li>Manuel Ignacio Pérez Alcolea</li>
          <li>Santiago Antonio Reartes</li>
        </List>
      </Wrapper>
    </Background>
  );
};

Help.propTypes = {
  handleButton: PropTypes.func.isRequired
};

export default Help;
