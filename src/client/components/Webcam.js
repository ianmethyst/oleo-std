import React from 'react';
import styled from 'styled-components';

class Webcam extends React.Component {
  constructor(props) {
    super(props);

    this.videoNode = React.createRef();

    this.videoConstraints = {
      audio: false,
      video: {
        width: 1280,
        height: 720,
       facingMode: 'environment'
      }
    };
  }

  componentDidMount() {
    navigator.mediaDevices.getUserMedia(this.videoConstraints)
      .then((mediaStream) => {
        const video = this.videoNode.current;
        video.srcObject = mediaStream;
        video.onloadedmetadata = (e) => {
          video.play();
        };
      })
      .catch((err) => {
        console.error(`${err.name} : ${err.message}`);
      });
  }

  render() {
    return (
      <video
        autoPlay
        muted
        ref={this.videoNode}
        style={{width: '100vw', height: '100vh', objectFit: 'cover', left: '0px', top: '0px', position: 'absolute'}}
      />
    );
  }
}

export default Webcam;
