import React from 'react';
import styled from 'styled-components';
import { hot } from 'react-hot-loader';

import { PoseWrapper } from './Elements';

import Container from './Container';

import Loading from './Loading';
import Main from './Main';
import Error from './Error';
import Help from './Help';
import Webcam from './Webcam';
import Three from './Three';

const views = {
  LOADING: 'LOADING',
  ERROR: 'ERROR',
  MAIN: 'MAIN',
  HELP: 'HELP'
};

const Wrapper = styled.div`
  position: relative;
`;

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      view: views.LOADING
    };

    this.handleLoad = this.handleLoad.bind(this);
    this.handleHelpButton = this.handleHelpButton.bind(this);
    this.handleBackButton = this.handleBackButton.bind(this);
  }

  getCurrentComponent() {
    const { view } = this.state;

    let element;
    switch (view) {
      case views.LOADING:
        element = <Loading />;
        break;
      case views.ERROR:
        element = <Error />;
        break;
      case views.MAIN:
        element = <Main handleButton={this.handleHelpButton} />;
        break;
      case views.HELP:
        element = <Help handleButton={this.handleBackButton} />;
        break;
      default:
        console.error('UNDEFINED VIEW');
        return null;
    }
    return (
      <PoseWrapper key={view}>
        {element}
      </PoseWrapper>
    );
  }

  handleBackButton() {
    this.setState({ view: views.MAIN });
  }

  handleHelpButton() {
    this.setState({ view: views.HELP });
  }

  handleLoad(success) {
    if (success) {
      this.setState({ view: views.MAIN });
    } else {
      this.setState({ view: views.ERROR });
    }
  }

  render() {
    return (
      <Wrapper>
        <Webcam />
        <Three handleLoad={this.handleLoad} x={0.3} y={-2} z={-12} />
        <Container>
          {this.getCurrentComponent()}
        </Container>
      </Wrapper>
    );
  }
}

export default hot(module)(App);
