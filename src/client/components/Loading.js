import React from 'react';

import { Spinner, Background } from './Elements';

const Load = () => (
  <Background>
    <Spinner color="white" />
  </Background>
);

export default Load;
