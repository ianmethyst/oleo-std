import styled from 'styled-components';
import posed from 'react-pose';
import { IntersectingCirclesSpinner } from 'react-epic-spinners';

import buttonBG from '../img/button_i.png';

export const Spinner = styled(IntersectingCirclesSpinner)`
  position: fixed;
  width: 100vw;
  height: 100vh;
  margin: 0 auto; /* Will not center vertically and won't work in IE6/7. */
  left: 0;
  right: 0;
`;

export const FrameImage = styled.img`
  position: fixed;
  height: 85vh;
  width: auto;
  margin: 0 auto; /* Will not center vertically and won't work in IE6/7. */
  top: 54%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export const Background = styled.div`
  position: fixed;
  width: 100vw;
  height 100vh;
  background: rgba(0, 0, 0, ${props => (props.opacity ? props.opacity : 1)});
  overflow: overlay;
  overflow-x: hidden;
`;

export const Wrapper = styled.div`
  display: flex;
  max-width: 720px;
  padding: 0 32px;
  margin: 0 auto;
  flex-direction: column;
`;

export const Header = styled.header`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
`;

export const Rule = styled.hr`
  width: 100%;
  border: 0;
  height: 1px;
  background-image: linear-gradient(to right, rgba(255, 255, 255, 0), rgba(255, 255, 255, 0.75), rgba(255, 255, 255, 0));
`;

export const Image = styled.img`
  max-width: 70%;
  height: auto;
  margin: 0 auto;
`;

export const Title = styled.h1`
  display: block;
  font-size: 2em;
  margin: 12px 0;
  padding: 0;
`;

export const Subtitle = styled.h2`
  display: block;
  font-size: 1.75em;
  margin: 24px 0 12px 0;
  padding: 0;
`;

export const List = styled.ul`
  list-style-type: none;
  margin: 0 0 24px 0;
  padding: 0;
  & li {
    font-size: 1em;
  }
`;

export const Text = styled.p`
  font-size: 1.25em;
`;

export const Button = styled.button`
  display: block;
  font-size: calc(12px + 3vh);
  line-height: calc(12px + 3vh);
  border: none;
  padding: calc(12px + 1vh);
  margin: 1vh auto 0 auto;
  color: white;
  background: none;
  background-image: url(${buttonBG});
  background-size: contain;
  background-position: 50% 50%;
  background-repeat: no-repeat;

`;

export const innerContainer = styled.div`
  max-width: 1070px;
  margin: 0 auto;
`;

export const PoseWrapper = posed.div({
  enter: { opacity: 1 },
  exit: { opacity: 0 }
});
