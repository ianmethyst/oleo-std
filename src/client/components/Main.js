import React from 'react';
import PropTypes from 'prop-types';

import frame from '../img/frame_i.png';

import {
  Button,
  Wrapper,
  FrameImage
} from './Elements';

const Main = (props) => {
  const { handleButton } = props;
  return (
    <Wrapper>
      <Button type="button" onClick={handleButton}>?</Button>
      <FrameImage src={frame} />
    </Wrapper>
  );
};

Main.propTypes = {
  handleButton: PropTypes.func.isRequired
};

export default Main;
