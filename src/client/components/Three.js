import React from 'react';
import PropTypes from 'prop-types';
import * as THREE from 'three';
import anime from 'animejs'
import io from 'socket.io-client';

import {
  buildRenderer,
  buildCamera,
  buildLights,
  buildScene
} from '../three/common';

import modelLoader from '../three/modelLoader';

class Three extends React.Component {
  constructor(props) {
    super(props);

    this.animate = this.animate.bind(this);
    this.onWindowResize = this.onWindowResize.bind(this);

    this.socket = io.connect('http://localhost:8080');

    window.addEventListener('resize', this.onWindowResize, false);
  }

  componentDidMount() {
    this.dimensions = {
      width: this.mount.clientWidth,
      height: this.mount.clientHeight
    };

    this.init();
    this.start();
  }

  componentWillUnmount() {
    this.stop();
    this.mount.removeChild(this.renderer.domElement);
  }

  onWindowResize() {
    this.dimensions = {
      width: this.mount.clientWidth,
      height: this.mount.clientHeight
    };

    const { width, height } = this.dimensions;

    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();

    this.renderer.setSize(width, height);
  }

  init() {
    const { handleLoad } = this.props;
    // Common elements
    this.clock = new THREE.Clock();
    this.camera = buildCamera(this.dimensions);
    this.renderer = buildRenderer(this.dimensions);
    this.scene = buildScene();
    this.scene.add(this.camera);
    this.lights = buildLights();
    this.lights.forEach((light) => { this.scene.add(light); });

    Promise.all([
      modelLoader('model/ghost.glb'),
      modelLoader('model/explode.glb')
    ]).then((models) => {
      models.forEach((model) => {
        if (model.animations.length === 3) {
          this.ghost = model.scene;
          this.ghostMixer = new THREE.AnimationMixer(this.ghost);
          this.clips = model.animations;

          console.log(this.clips);


          this.painting = THREE.AnimationClip.findByName(this.clips, 'painting');
          this.breathing = THREE.AnimationClip.findByName(this.clips, 'breathing');
          this.thinking = THREE.AnimationClip.findByName(this.clips, 'thinking');

          this.ghost.traverse((child) => {
            if (child.material && child.material.name === 'marble') {
              child.material.setValues({
                color: '#f7f7f7',
                emissive: '#000000',
                roughness: 0.62,
                metalness: 0,
                transparent: true,
                opacity: 0.1
              });
              this.ghostMaterial = child.material;
            }
          });

          const { x, y, z } = this.props;

          this.ghost.position.set(x, y, z);
          this.ghost.rotation.set(0, Math.PI * 1.25, 0);

          Object.assign(this.ghost, THREE.EventDispatcher);

          this.ghost.addEventListener('add', (e) => {
            console.log('ghost added');
            this.scene.add(this.ghost);
          });

          this.ghost.addEventListener('reset', (e) => {
            console.log('ghost resetted');
            this.ghostMixer.stopAllAction();
            this.ghost.dispatchEvent({ type: 'breath' });
            this.ghostMaterial.setValues({
              opacity: 0.1
            });
            this.scene.add(this.ghost);
          });


          this.ghost.addEventListener('remove', (e) => {
            console.log('ghost removed');
            this.scene.remove(this.ghost);
          });

          this.ghost.addEventListener('appear', (e) => {
            console.log('Started appearing');
            anime({
              targets: this.ghostMaterial,
              opacity: 0.6,
              easing: 'easeInOutQuart',
              duration: 4000,
              complete: (anim) => {
                if (anim.completed) {
                  console.log('Finished appearing');
                  if (!e.finale) {
                    this.ghost.dispatchEvent({ type: 'paint' });
                  } else {
                    this.ghost.dispatchEvent({ type: 'think' });
                  }
                }
              }
            });
          });

          this.ghost.addEventListener('disappear', (e) => {
            console.log('Started disappearing');
            anime({
              targets: this.ghostMaterial,
              opacity: 0.1,
              easing: 'easeInOutQuart',
              duration: 4000,
              complete: (anim) => {
                if (anim.completed) {
                  console.log('Finished disappearing');
                  this.ghost.dispatchEvent({ type: 'breathing' });
                }
              }
            });
          });

          this.ghost.addEventListener('breath', (e) => {
            console.log('Started breathing');
            if(this.ghostAction) {
              this.ghostAction.stop();
            }
            this.ghostAction = this.ghostMixer.clipAction(this.breathing);
            this.ghostAction.play();
            this.ghostAction.loop = THREE.LoopPingPong;
          });

          this.ghost.addEventListener('paint', (e) => {
            console.log('Started painting');
            this.ghostAction.stop();
            this.ghostAction = this.ghostMixer.clipAction(this.painting);
            this.ghostAction.reset();
            this.ghostAction.play();
            this.ghostAction.loop = THREE.LoopOnce;
            this.ghostMixer.addEventListener('finished', (e) => {
              this.ghost.dispatchEvent({ type: 'disappear' });
              this.ghost.dispatchEvent({ type: 'breath' });
              
              this.ghostMixer.removeEventListener('finished');
            });
          });

          this.ghost.addEventListener('think', (e) => {
            console.log('Started thinking');
            this.ghostAction.stop();
            this.ghostAction = this.ghostMixer.clipAction(this.thinking);
            this.ghostAction.play();
            this.ghostAction.loop = THREE.LoopOnce;
            this.ghostAction.clampWhenFinished = true;
            this.ghostMixer.addEventListener('finished', (e) => {
              this.ghost.dispatchEvent({ type: 'petrify' });
              this.ghostMixer.removeEventListener('finished');
            });
          });

          this.ghost.addEventListener('petrify', (e) => {
            console.log('Started petrify');
            setTimeout(() => {
              this.ghost.dispatchEvent({ type: 'remove' });
              this.fractured.dispatchEvent({ type: 'add' });
              this.fractured.dispatchEvent({ type: 'explode' });
              this.ghostMixer.removeEventListener('finished');
            }, 3000);
          });

          this.socket.on('osc', (data) => {
            if (data.address === '/animate') {
              const arg = data.args[0];
              if (arg.type === 's') {
                switch (arg.value) {
                  case 'equity':
                  case 'brotherhood':
                  case 'liberty': {
                    this.ghost.dispatchEvent({ type: 'appear', finale: false });
                    break;
                  }
                  case 'finale': {
                    this.ghost.dispatchEvent({ type: 'appear', finale: true });
                    break;
                  }
                  default: {
                    console.log('OSC: reached default case');
                  }
                }
              }
            }
          });

          this.ghost.dispatchEvent({type: 'add' });
          this.ghost.dispatchEvent({type: 'breath' });
        } else {
          this.fractured = model.scene;

          const { x, y, z } = this.props;
          this.fractured.position.set(x, y, z);
          this.fractured.rotation.set(0, Math.PI * 1.25, 0);

          Object.assign(this.fractured, THREE.EventDispatcher);

          this.fracturedMixer = new THREE.AnimationMixer(this.fractured);
          this.fracturedClips = model.animations;

          this.fractured.traverse((child) => {
            if (child.material && child.material.name === 'marble') {
              child.material.setValues({
                color: '#f7f7f7',
                emissive: '#000000',
                roughness: 0.62,
                metalness: 0,
                transparent: true,
                opacity: 1
              });
              this.fracturedMaterial = child.material;
            }
          });

          this.fractured.addEventListener('explode', (e) => {
            console.log('Exploding');
            this.fracturedClips.forEach((clip) => {
              console.log(clip);
              this.fracturedAction = this.fracturedMixer.clipAction(clip);
              this.fracturedAction.play();
              setTimeout(() => {
                this.fractured.dispatchEvent({ type: 'disappear' });
              }, 4000);
            });
          });

          this.fractured.addEventListener('add', (e) => {
            console.log('Fractured added');
            this.scene.add(this.fractured);
          });

          this.fractured.addEventListener('remove', (e) => {
            console.log('Fractured removed');
            this.scene.remove(this.fractured);
          });

          this.fractured.addEventListener('disappear', (e) => {
            console.log('Started disappearing');
            anime({
              targets: this.fracturedMaterial,
              opacity: 0,
              easing: 'easeInOutQuart',
              duration: 4000,
              complete: (anim) => {
                if (anim.completed) {
                  console.log('Finished disappearing');
                  this.fractured.dispatchEvent({ type: 'remove' });
                  this.ghost.dispatchEvent({ type: 'reset' });
                  this.ghost.dispatchEvent({ type: 'add' });
                }
              }
            });
          });
        }
        handleLoad(true);
      });
    });

    // Append canvas
    this.mount.appendChild(this.renderer.domElement);
  }

  componentDidUpdate() {
    const { x, y, z } = this.props;
    if (this.ghost) {
      this.ghost.position.set(x, y, z);
    }
  }


  start() {
    if (!this.frameId) {
      this.frameId = requestAnimationFrame(this.animate);
    }
  }

  stop() {
    cancelAnimationFrame(this.frameId);
  }

  animate() {
    this.renderScene();
    this.frameId = window.requestAnimationFrame(this.animate);

    const deltaTime = this.clock.getDelta();

    if (this.ghostMixer) {
      this.ghostMixer.update(deltaTime);
    }
    if (this.fracturedMixer) {
      this.fracturedMixer.update(deltaTime);
    }
  }

  renderScene() {
    this.renderer.render(this.scene, this.camera);
  }

  // React render
  render() {
    return (
      <div
        id="three"
        style={{ position: 'absolute', width: '100vw', height: '100vh' }}
        ref={(mount) => { this.mount = mount; }}
      />
    );
  }
}

Three.propTypes = {
  handleLoad: PropTypes.func.isRequired
};

export default Three;
