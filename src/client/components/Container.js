import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { PoseGroup } from 'react-pose';

const Wrapper = styled.div`
  position: relative;
  width: 100vw;
  height: 100vh;
`;

const Container = ({ children }) => (
  <Wrapper>
    <PoseGroup>
      {children}
    </PoseGroup>
  </Wrapper>
);

Container.propTypes = {
  children: PropTypes.node.isRequired
};

export default Container;
