# STD

Componente de STD para Trabajo Práctico 5 de Lenguaje Multimedial III. Año 2018.

## ¿Cómo instalar?

En primer lugar, se debe clonar el repositorio:

```sh
$ git clone https://gitlab.com/ianmethyst/oleo-std/
```

Para poder instalar los paquetes de node y ejecutar los scripts, se debe instalar [Node.js](https://nodejs.org/es/) y [Yarn](https://yarnpkg.com/es-ES/).

Una vez clonado el repositorio, instalamos las dependencias con Yarn:

```sh
$ yarn install 
```

## ¿Cómo usar?

En el archivo `package.json` hay varios scripts definidos para automatizar diferentes tareas.

Correr el servidor de desarrollo:

```shell
$ yarn dev
```

Compilar una versión de producción:

```shell
$ yarn dist
```

